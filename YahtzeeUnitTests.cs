using NUnit.Framework;

namespace yahtzee
{
    public class YahtzeeUnitTests
    {
        private Scoreboard _sb;

        [SetUp]
        public void Setup()
        {
            // The dice roll in the unit tests are the final rolls of the players turn. 
            _sb = new Scoreboard();
        }

        [Test]
        public void DiceValueOneScoreTwoTest()
        {
            int[] diceTest = new int[5] { 1, 3, 2, 1, 6 };
            Assert.AreEqual(_sb.CalculateValueX(diceTest, 1), 2);
        }

        [Test]
        public void DiceValueTwoScoreFourTest()
        {
            int[] diceTest = new int[5] { 1, 2, 5, 2, 6 };
            Assert.AreEqual(_sb.CalculateValueX(diceTest, 2), 4);
        }

        [Test]
        public void DiceValueThreeScoreSixTest()
        {
            int[] diceTest = new int[5] { 3, 3, 5, 5, 6 };
            Assert.AreEqual(_sb.CalculateValueX(diceTest, 3), 6);
        }

        [Test]
        public void DiceValueFourScoreEightTest()
        {
            int[] diceTest = new int[5] { 4, 1, 5, 6, 4 };
            Assert.AreEqual(_sb.CalculateValueX(diceTest, 4), 8);
        }

        [Test]
        public void DiceValueFiveScoreFiveTest()
        {
            int[] diceTest = new int[5] { 2, 2, 5, 2, 6 };
            Assert.AreEqual(_sb.CalculateValueX(diceTest, 5), 5);
        }

        [Test]
        public void DiceValueSixScoreTwelveTest()
        {
            int[] diceTest = new int[5] { 3, 3, 5, 6, 6 };
            Assert.AreEqual(_sb.CalculateValueX(diceTest, 6), 12);
        }

        [Test]
        public void ThreeOfaKindScoreTwelveTest()
        {
            int[] diceTest = new int[5] { 3, 3, 3, 6, 6 };
            Assert.AreEqual(_sb.CalculateXOfaKind(diceTest, 3), 21);
        }

        [Test]
        public void FourOfaKindScoreTwentySixTest()
        {
            int[] diceTest = new int[5] { 5, 5, 5, 5, 6 };
            Assert.AreEqual(_sb.CalculateXOfaKind(diceTest, 3), 26);
        }

        [Test]
        public void FourOfaKindScoreZeroTest()
        {
            int[] diceTest = new int[5] { 3, 3, 3, 5, 6 };
            Assert.AreEqual(_sb.CalculateXOfaKind(diceTest, 4), 0);
        }

        [Test]
        public void YahtzeeScoreFiftyTest()
        {
            int[] diceTest = new int[5] { 2, 2, 2, 2, 2 };
            Assert.AreEqual(_sb.CalculateXOfaKind(diceTest, 5), 50);
        }

        [Test]
        public void FullHouseExistTest()
        {
            int[] diceTest = new int[5] { 2, 2, 5, 5, 5 };
            Assert.AreEqual(_sb.CalculateFullHouse(diceTest), 25);
        }

        [Test]
        public void FullHouseNotExistTest()
        {
            int[] diceTest = new int[5] { 2, 4, 2, 3, 2 };
            Assert.AreEqual(_sb.CalculateFullHouse(diceTest), 0);
        }

        [Test]
        public void LargeStraightDice23456Test()
        {
            int[] diceTest = new int[5] { 2, 4, 5, 3, 6 };
            Assert.AreEqual(_sb.CalculateStraight(diceTest, true), 40);
        }

        [Test]
        public void LargeStraightDice12345Test()
        {
            int[] diceTest = new int[5] { 1, 5, 3, 4, 2 };
            Assert.AreEqual(_sb.CalculateStraight(diceTest, true), 40);
        }

        [Test]
        public void SmallStraightDice1234Test()
        {
            int[] diceTest = new int[5] { 2, 1, 4, 3, 6 };
            Assert.AreEqual(_sb.CalculateStraight(diceTest, false), 30);
        }

        [Test]
        public void SmallStraightDice2345Test()
        {
            int[] diceTest = new int[5] { 3, 5, 6, 4, 2 };
            Assert.AreEqual(_sb.CalculateStraight(diceTest, false), 30);
        }

        [Test]
        public void SmallStraightDice3456Test()
        {
            int[] diceTest = new int[5] { 1, 5, 3, 4, 6 };
            Assert.AreEqual(_sb.CalculateStraight(diceTest, false), 30);
        }

        public void SmallStraightDice33456Test()
        {
            int[] diceTest = new int[5] { 3, 5, 3, 4, 6 };
            Assert.AreEqual(_sb.CalculateStraight(diceTest, false), 30);
        }

        [Test]
        public void SmallLargeStraightNotExistTest()
        {
            int[] diceTest = new int[5] { 1, 6, 3, 4, 6 };
            Assert.AreEqual(_sb.CalculateStraight(diceTest, false), 0);
        }

        [Test]
        public void ChanceScoreTwentyFourTest()
        {
            int[] diceTest = new int[5] { 6, 4, 6, 3, 5 };
            Assert.AreEqual(_sb.CalculateChance(diceTest), 24);
        }
    }
}