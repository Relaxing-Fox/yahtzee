using System;

namespace yahtzee
{
    class Scoreboard
    {
        // To be used throughout the Service class.
        private int sum = 0;

        /// <summary>
        /// CalculateValueX determines the total amount of a specified die value in a dice collection.
        /// </summary>
        /// <param name="dice">The collection of dice.</param>
        /// <param name="value">Value of the dice to be totalled (1s, 2s, ... 6s).</param>
        /// <returns>The total of the specified die value in the collection of dice.</returns>
        public int CalculateValueX(int[] dice, int value)
        {
            // Loops through the dice array.
            for (int a = 0; a < dice.Length; a++)
            {
                // Sorts the dice array.
                Array.Sort(dice);

                // If dice is the value of (1, 2, 3, 4, 5, 6).
                if (dice[a] == value)
                {
                    // Sums up all the dice
                    sum += dice[a];
                }
                else
                {
                    // Dice value is not in the specified value.
                    continue;
                }
            }

            return sum;
        }

         /// <summary>
        /// CalculateXOfaKind determines the total amount of a specified die value in a dice collection.
        /// </summary>
        /// <param name="dice">The collection of dice.</param>
        /// <param name="value">Value of the X of a kind (3, 4, or 5 "Yahtzee").</param>
        /// <returns>
        /// The total of the specified die value in the collection of dice if 3 or 4 of a kind or Yahtzee.
        /// </returns>
        public int CalculateXOfaKind(int[] dice, int valueOfaKind)
        {
            int sum = 0;
            int counter = 1;
            bool isXOfaKind = false;

            // Sorts the dice array.
            Array.Sort(dice);

            // Loops through the dice array.
            for (int i = 0; i < dice.Length; i++)
            {
                // Sums up all the dice
                sum += dice[i];

                if (i < dice.Length - 1)
                {
                    // When two consecutive die values are the same then increment the counter.
                    if (dice[i] == dice[i + 1])
                    {
                        counter++;

                        // If the counter is the X of a kind (3, 4, or 5) then flag it.
                        if (valueOfaKind == counter)
                        {
                           isXOfaKind = true;
                        }
                    }
                    else
                    {
                        // Two consecutive die values do not equate, start the counter over.
                        counter = 1;
                    }
                }
            }

            if (!isXOfaKind)
            {
                // Not a X of a kind.
                sum = 0;
            }
            else
            {
                // When X is 5 of a kind then adjust score for Yahtzee.
                if (valueOfaKind == 5)
                {
                    sum = 50;
                }
            }
            return sum;
        } 

       /// <summary>
        /// Calculates whether or not a full house (2 of a kind + 3 of a kind) exists.
        /// </summary>
        /// <param name="dice">The collection of dice.</param>
        /// <returns>If a full house exists, the, total is 25, or 0.</returns>
        public int CalculateFullHouse(int[] dice)
        {
            bool isSetOf2 = false;
            bool isSetOf3 = false;
            bool isFullHouse = true;

            // sort the dice array
            Array.Sort(dice);

            for (int i = 0; i < dice.Length; i++)
            {
                if (i < dice.Length - 1)
                {
                    switch (i)
                    {
                        case 0:
                            if (dice[i] != dice[i + 1])
                            {
                                // full house not possible - in a sorted array, the first 2 values MUST be equal for a full house to exist
                                isFullHouse = false;
                            }
                            break;
                        case 1:
                            if (dice[i] == dice[i + 1])
                            {
                                // a set of 3 has been made
                                isSetOf3 = true;
                            }
                            else
                            {
                                // a set of 2 has been made
                                isSetOf2 = true;
                            }
                            break;
                        case 2:
                            if (dice[i] == dice[i + 1])
                            {
                                if (isSetOf3)
                                {
                                    // full house not possible - due to 4 of a kind
                                    isFullHouse = false;
                                }
                            }
                            else
                            {
                                if (isSetOf2)
                                {
                                    // full house not possible - due to more than two values represented in the array of dice
                                    isFullHouse = false;
                                }
                            }
                            break;
                        case 3:
                            if (dice[i] != dice[i + 1])
                            {
                                // full house not possible - in a sorted array, the last 2 values MUST be equal for a full house to exist
                                isFullHouse = false;
                            }
                            break;
                        default:
                            // do nothing
                            break;
                    }
                }

                if (!isFullHouse)
                {
                    // full house not possible, stop analyzing the dice, no score
                    break;
                }
            }

            if (isFullHouse)
            {
                // full house determined
                sum = 25;
            }

            return sum;
        }

        /// <summary>
        /// Calculate whether or not it is a large or small straight.
        /// </summary>
        /// <param name="dice">The collection of dice.</param>
        /// <param name="wantLargeStraight">True for large straight and false for small straight.</param>
        /// <returns>The total of 40 if a large straight exists, and 30 if a small straight exists, or 0.</returns>
        public int CalculateStraight(int[] dice, bool wantLargeStraight)
        {
            bool isStraight = true;
            bool smallStraightPassUsed = false;

            // sort the dice array
            Array.Sort(dice);

            for (int i = 0; i < dice.Length; i++)
            {
                if (i < dice.Length - 1)
                {
                    switch (i)
                    {
                        case 0:
                            if (dice[i] != dice[i + 1] - 1)
                            {
                                if (wantLargeStraight)
                                {
                                    // large straight not possible - each of the 5 dice values MUST be consecutively one less than the next
                                    isStraight = false;
                                }
                                else
                                {
                                    smallStraightPassUsed = true;
                                }
                            }
                            break;
                        case 1:
                            // When two consecutive die values are the same.
                            if (dice[i] == dice[i + 1])
                            {
                                continue;
                            }
                            break;
                        case 2:
                            if (dice[i] != dice[i + 1] - 1)
                            {
                                // straight not possible - the first 4 dice OR, the last 4 dice, values MUST be consecutively one less than the next
                                isStraight = false;
                            }
                            break;
                        case 3:
                            if (dice[i] != dice[i + 1] - 1)
                            {
                                if (wantLargeStraight)
                                {
                                    // large straight not possible - each of the 5 dice values MUST be consecutively one less than the next
                                    isStraight = false;
                                }
                                else
                                {
                                    if (smallStraightPassUsed)
                                    {
                                        // small straight not possible - must have 4 consecutive values of one less than the next
                                        isStraight = false;
                                    }
                                }
                            }
                            break;
                        }
                }

                if (!isStraight)
                {
                    // straight not possible, stop analyzing the dice, no score
                    break;
                }
            }

            if (isStraight)
            {
                // straight determined
                if (wantLargeStraight)
                {
                    sum = 40;
                }
                else
                {
                    sum = 30;
                }
            }

            return sum;
        }

        public int CalculateChance(int[] dice)
        {
            // sort the dice array
            Array.Sort(dice);

            for (int a = 0; a < dice.Length; a++)
            {
                sum += dice[a];
            }
            return sum;
        }
    }
}