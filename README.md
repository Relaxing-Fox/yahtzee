# Yahtzee

The objective of the game is to score points by rolling five dice to make certain combinations. 
The dice can be rolled up to three times in a turn to try to make various scoring combinations and dice must remain in the box.
A game consists of thirteen rounds.
After each round the player chooses which scoring category is to be used for that round.
Once a category has been used in the game, it cannot be used again.
The scoring categories have varying point values,
some of which are fixed values and others for which the score depends on the value of the dice.
The winner is the player who scores the most points.

## Upper Section
| Categories | Descriptions | Scores | Examples |
| ------ | ------ | ------ | ------ |
| Aces | Any combination | The sum of dice with the number one	 | (1-3-1-1-6) scores 3 |
| Twos | Any combination | The sum of dice with the number two	 | (4-2-5-3-2) scores 4 |
| Threes | Any combination | The sum of dice with the number three	 | (4-3-5-3-3) scores 9 |
| Fours | Any combination | The sum of dice with the number four | (4-4-5-3-4) scores 12 |
| Fives | Any combination | The sum of dice with the number five	 | (1-2-5-5-2) scores 10 |
| Sixes | Any combination | The sum of dice with the number six	 | (4-6-6-3-6) scores 18 |

## Lower Section
| Categories | Descriptions | Scores | Examples |
| ------ | ------ | ------ | ------ |
| 3 Of A Kind | At least three dice the same | Sum of all dice | (5-2-5-5-2) scores 19 |
| 4 Of A Kind | At least four dice the same | Sum of all dice | (3-3-3-3-2) scores 14 |
| Full House | Three of one number and two of another | 25 | (4-3-3-3-4) scores 4 |
| Small Straight | Four sequential dice (1-2-3-4, 2-3-4-5, or 3-4-5-6) | 30 | (4-2-5-3-2) scores 4 |
| Large Straight | Five sequential dice (1-2-3-4-5 or 2-3-4-5-6) | 40 | (4-2-5-3-6) scores 4 |
| Yahtzee (5 of a Kind) | All five dice the same | 50 | (5-5-5-5-5) scores 4 |
| Chance | Any combination | Sum of all dice | (6-1-5-3-2) scores 17 |

### Tasks
*  Create a function that can calculates values for the Upper Section. (Completed)
*  Create a function that can calculates for 3 of a Kind. (Completed)
*  Create a function that can calculates for 4 of a Kind. (Completed)
*  Create a function that can calculates for Full House. (Completed)
*  Create a function that can calculates for Small and Large Straight. (Completed)
*  Create a function that can calculates for Yahtzee (5 Of A Kind). (Completed)
*  Create a function that can calculates for Chance. (Completed)



